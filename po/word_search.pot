# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-25 11:43+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: word_search.cpp:1308 word_search.cpp:1324
msgid ""
"\n"
"The default parameters are stored"
msgstr ""

#: word_search.cpp:297
msgid " and column : "
msgstr ""

#: word_search.cpp:297
msgid " don't exist"
msgstr ""

#: word_search_solve.cpp:62
msgid " is not present"
msgstr ""

#: dialog_options.cpp:35
msgid "ASCII character"
msgstr ""

#: dialog_options.cpp:33
msgid "Blacklist of character"
msgstr ""

#: dictionary.cpp:47
msgid "Can't open the dict file"
msgstr ""

#: dialog_new.cpp:16 dialog_ocr.cpp:17 dialog_options.cpp:49
msgid "Cancel"
msgstr ""

#: word_search.cpp:1347
msgid "Cannot create the .word_search directory\n"
msgstr ""

#: word_search.cpp:1322
msgid ""
"Cannot open the ini file.\n"
"Error is : "
msgstr ""

#: word_search.cpp:1306
msgid ""
"Cannot open the key.\n"
"Error is : "
msgstr ""

#: word_search.cpp:1373
msgid ""
"Cannot save to the ini file.\n"
"Error is : "
msgstr ""

#: word_search.cpp:67
msgid "Computing Solve"
msgstr ""

#: word_search.cpp:71
msgid "Create"
msgstr ""

#: word_search.cpp:100
msgid "Create this variable"
msgstr ""

#: word_search.cpp:1116
msgid "Created by"
msgstr ""

#: dialog_options.cpp:34
msgid "Dictionnary Path"
msgstr ""

#: word_search.cpp:124
msgid "Directory problem"
msgstr ""

#: word_search.cpp:102
msgid "Environment variables problem"
msgstr ""

#: word_search.cpp:60
msgid "File"
msgstr ""

#: word_search.cpp:68
msgid "Generate grid by the computer"
msgstr ""

#: word_search.cpp:60
msgid "Help"
msgstr ""

#: dialog_new.cpp:6
msgid "How many columns"
msgstr ""

#: dialog_new.cpp:6
msgid "How many lines"
msgstr ""

#: dialog_new.cpp:6
msgid "How many words"
msgstr ""

#: word_search_solve.cpp:61
msgid "It's impossible to solve the grid. The word "
msgstr ""

#: dialog_options.cpp:31
msgid "Language"
msgstr ""

#: word_search.cpp:66
msgid "Manual Solve"
msgstr ""

#: word_search.cpp:60
msgid "Mode"
msgstr ""

#: word_search.cpp:389
msgid "OCR initialisation fail"
msgstr ""

#: dialog_new.cpp:15 dialog_ocr.cpp:16 dialog_options.cpp:48
msgid "OK"
msgstr ""

#: word_search.cpp:142
msgid "Open"
msgstr ""

#: word_search.cpp:70
msgid "Open OCR files"
msgstr ""

#: word_search.cpp:530
msgid "Open a Word Search file"
msgstr ""

#: dialog_ocr.cpp:94
msgid "Open a picture for the grid letters"
msgstr ""

#: dialog_ocr.cpp:104
msgid "Open a picture for the grid words"
msgstr ""

#: word_search.cpp:69
msgid "Open text files"
msgstr ""

#: dialog_ocr.cpp:12
msgid "Path of the picture file for the letters"
msgstr ""

#: dialog_ocr.cpp:12
msgid "Path of the picture file for the words"
msgstr ""

#: dialog_options.cpp:44
msgid "Path to the dictionnary file"
msgstr ""

#: word_search.cpp:1350
msgid "Problem to create directory"
msgstr ""

#. Else error
#: word_search.cpp:121
msgid "Problem to find the software path\n"
msgstr ""

#: word_search.cpp:178
msgid "Progress compute words"
msgstr ""

#: word_search.cpp:267
msgid "Save a Word Search file"
msgstr ""

#: word_search.cpp:991
msgid "The character inserted is not a letter"
msgstr ""

#: word_search.cpp:122 word_search.cpp:1348
msgid "The software stopped"
msgstr ""

#. Else error message
#: word_search.cpp:99
msgid "The variable named %1 doesn't exist\n"
msgstr ""

#: word_search.cpp:297
msgid "The widget at line : "
msgstr ""

#: word_search.cpp:478
msgid "This lines are not correct : "
msgstr ""

#: word_search.cpp:447
msgid "This words are not correct : "
msgstr ""

#: dialog_options.cpp:32
msgid "Whitelist of character"
msgstr ""

#: word_search.cpp:299
msgid "Widget problem"
msgstr ""

#: word_search.cpp:60
msgid "Word Search"
msgstr ""

#: word_search.cpp:72
msgid "Word Search Grid"
msgstr ""

#: word_search.cpp:1118
msgid "Word Search is a program to solve Word Search grid"
msgstr ""

#: word_search.cpp:73
msgid "Words"
msgstr ""

#: dialog_ocr.cpp:76
msgid "pictures files"
msgstr ""
