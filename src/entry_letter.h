#ifndef ENTRY_LETTER_H
#define ENTRY_LETTER_H
#include "glibmm/refptr.h"
#include "gtkmm/cssprovider.h"
#include "gtkmm/eventbox.h"
#include <gtkmm/entry.h>
#include <string>
#include <vector>

template <typename Letter>
Letter* get_letter_widget(Gtk::EventBox *event)
{
     Letter *widget=nullptr;
     if (event!=nullptr)
     {
	  std::vector<Gtk::Widget*> vec_entry=event->get_children();
	  widget=(Letter*)vec_entry.at(0);
     }
     return widget;
}

class Entry_Letter : public Gtk::Entry
{
public:
     Entry_Letter();
     ~Entry_Letter();
private:
};
#endif
