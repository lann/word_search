#include "dialog_ocr.h"
#include "drawingarea_letter.h"
#include "entry_letter.h"
#include "gdkmm/cursor.h"
#include "gdkmm/device.h"
#include "gdkmm/display.h"
#include "gdkmm/pixbuf.h"
#include "gdkmm/window.h"
#include "glibmm/fileutils.h"
#include "glibmm/keyfile.h"
#include "glibmm/refptr.h"
#include "gtkmm/assistant.h"
#include "gtkmm/eventbox.h"
#include "gtkmm/image.h"
#include "gtkmm/label.h"
#include "gtkmm/textview.h"
#include "gtkmm/widget.h"
#include "drawingarea_letter.h"
#include "word_search.h"
#include "glibmm/ustring.h"
#include "gtkmm/box.h"
#include "gtkmm/entry.h"
#include "gtkmm/enums.h"
#include "gtkmm/filechooser.h"
#include "gtkmm/grid.h"
#include "gtkmm/messagedialog.h"
#include "gtkmm/stock.h"
#include "gtkmm/stockid.h"
#include "sigc++/adaptors/bind.h"
#include "sigc++/functors/mem_fun.h"
#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <map>
#include <ostream>
#include <sstream>
#include <tesseract/baseapi.h>
#include <leptonica/allheaders.h>
#include <fstream>
#include "word_search_solve.h"
#include <cstddef>
#include <ios>
#include <iostream>
#include <random>
#include <iomanip>
#include <libintl.h>
#include <string>
#include <tesseract/pageiterator.h>
#include <utility>
#include <vector>
#include <gdkmm.h>
#include <regex>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#define _(String) gettext(String)

Word_Search::Word_Search()
     : m_menuitem_file(_("File")), m_menuitem_help(_("Help")), m_menuitem_word_search(_("Word Search")), m_menuitem_mode(_("Mode")),
       m_menu_image_quit(Gtk::StockID("gtk-quit")),
       m_menu_image_new(Gtk::StockID("gtk-new")),
       m_menu_image_save_as(Gtk::StockID("gtk-save-as")),
       m_menu_image_save(Gtk::StockID("gtk-save")),
       m_menu_image_options(Gtk::StockID("gtk-properties")),
       m_menu_image_about(Gtk::StockID("gtk-about")),
       m_menu_image_manual_solve(_("Manual Solve")),
       m_menu_image_computing_solve(_("Computing Solve")),
       m_menu_image_generate(_("Generate grid by the computer")),
       m_menu_image_open_text(_("Open text files")),
       m_menu_image_open_ocr(_("Open OCR files")),
       m_check_menu_item_create(_("Create")),
       m_lbl_tab(_("Word Search Grid"),Gtk::ALIGN_START,Gtk::ALIGN_START),
       m_lbl_words(_("Words"),Gtk::ALIGN_START,Gtk::ALIGN_START), new_words_count(0), new_lines_count(0), new_columns_count(0),m_manual_solve(false), m_word_choice(false){
#ifdef HAVE_CONFIG_H
     m_sz_share = GLADE_DIR;
#endif
     if (m_sz_share != "") {
          std::string szicon(m_sz_share + "/word_search.ico");
          set_icon(Gdk::Pixbuf::create_from_file(szicon));
          m_pic_solve.set(m_sz_share+"/solve.png");
	  m_image_manual_solve.set(m_sz_share+"/edit.png");
          m_menu_image_computing_solve.set_image(m_pic_solve);
	  m_menu_image_manual_solve.set_image(m_image_manual_solve);
     }
#ifdef __linux__
     const char *home; //Pointer to the char
     home=secure_getenv("HOME");
     if (home!=NULL) //If home
     {
          m_szparameters_ini=home;
          m_sz_home=home;
          m_szparameters_ini+="/.word_search/word_search.ini";  //And the ini file directory
          m_sz_home+="/.word_search";
          open_file_parameters(m_szparameters_ini);
     }
     else
     {
          //Else error message
          Glib::ustring message=Glib::ustring::compose(_("The variable named %1 doesn't exist\n"),"$HOME");
          message+=_("Create this variable");
          Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
          Dialog.set_title(_("Environment variables problem"));
          Dialog.run();
          on_Quit();
     }
#elif defined (_WIN32)
     unsigned long result;
     char cHome[MAX_PATH];
//For  windaube, get the current directory
     result=GetCurrentDirectory(MAX_PATH,cHome);
     if ((result) && (result<MAX_PATH))
     {
          //and the directory of ini file
          m_szparameters_ini=cHome;
          m_szparameters_ini="\\word_search.ini";
          open_file_parameters(m_szparameters_ini);
     }
     else
     {
          //Else error
          std::string message(_("Problem to find the software path\n")); 
          message+=_("The software stopped");
          Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
          Dialog.set_title(_("Directory problem"));
          Dialog.run();
          on_Quit();
     }
#endif     
     m_dlg_open=nullptr;
     m_vec_color=nullptr;
     m_dlg_ocr=nullptr;
     m_grid_tab = new Gtk::Grid;
     m_grid_words=new Gtk::Grid;
     add_events(Gdk::ALL_EVENTS_MASK);
     m_menuitem_file.set_submenu(m_menu_file);
     m_menuitem_help.set_submenu(m_menu_help);
     m_menuitem_word_search.set_submenu(m_menu_word_search);
     m_menuitem_mode.set_submenu(m_menu_mode);
     m_menu_file.append(m_menu_image_new);
     m_menu_file.append(m_menu_item_open);
     m_menu_item_open.set_submenu(m_menu_file_open);
     m_menu_item_open.set_label(_("Open"));
     m_menu_file_open.append(m_menu_image_open_text);
     m_menu_file_open.append(m_menu_image_open_ocr);
     m_menu_file.append(m_menu_image_save_as);
     m_menu_file.append(m_menu_image_save);
     m_menu_file.append(m_menu_image_options);
     m_menu_file.append(m_menu_image_quit);
     m_menu_help.append(m_menu_image_about);
     m_menu_word_search.append(m_menu_image_generate);
     m_menu_word_search.append(m_menu_image_computing_solve);
     m_menu_word_search.append(m_menu_image_manual_solve);
     m_menu_mode.append(m_check_menu_item_create);
     m_check_menu_item_create.set_active();
     m_menubar.append(m_menuitem_file);
     m_menubar.append(m_menuitem_mode);
     m_menubar.append(m_menuitem_word_search);
     m_menubar.append(m_menuitem_help);
     m_menu_image_quit.signal_activate().connect(
          sigc::mem_fun(*this, &Word_Search::on_Quit));
     m_menu_image_new.signal_activate().connect(
          sigc::mem_fun(*this, &Word_Search::on_New));
     m_menu_image_save.signal_draw().connect(sigc::mem_fun(*this,&Word_Search::on_draw_menu_save));
     m_menu_image_open_text.signal_activate().connect(sigc::mem_fun(*this,&Word_Search::on_open_text_files));
     m_menu_image_open_ocr.signal_activate().connect(sigc::mem_fun(*this, &Word_Search::on_open_ocr_files));
     m_menu_image_save.signal_activate().connect(sigc::mem_fun(*this,&Word_Search::on_save));
     m_menu_image_save_as.signal_activate().connect(sigc::mem_fun(*this,&Word_Search::on_save_as));
     m_menu_image_options.signal_activate().connect(sigc::mem_fun(*this, &Word_Search::on_options));
     m_check_menu_item_create.signal_toggled().connect(sigc::mem_fun(*this,&Word_Search::on_toggled_check_create));
     m_check_menu_item_create.signal_draw().connect(sigc::mem_fun(*this, &Word_Search::on_draw_menu_check_create));
     m_menu_image_about.signal_activate().connect(
          sigc::mem_fun(*this, &Word_Search::on_Dialog_About));
     m_menu_image_computing_solve.signal_draw().connect(sigc::mem_fun(*this,&Word_Search::on_Draw_Menu_Solve));
     m_menu_image_generate.signal_activate().connect(sigc::mem_fun(*this,&Word_Search::on_generate));
     m_menu_image_generate.signal_draw().connect(sigc::mem_fun(*this,&Word_Search::on_draw_menu_generate));
     m_menu_image_manual_solve.signal_draw().connect(sigc::mem_fun(*this,&Word_Search::on_Draw_Menu_Solve));
     m_menu_image_computing_solve.signal_activate().connect(sigc::mem_fun(*this,&Word_Search::on_computing_solve));
     m_menu_image_manual_solve.signal_activate().connect(sigc::mem_fun(*this,&Word_Search::on_manual_solve));
     signal_show().connect(sigc::mem_fun(*this,&Word_Search::on_Show));
     m_progress_bar.set_text(_("Progress compute words"));
     m_progress_bar.set_show_text();
     m_progress_bar.set_valign(Gtk::ALIGN_CENTER);
     m_progress_bar.set_halign(Gtk::ALIGN_START);
     VBox1.pack_start(m_menubar, Gtk::PACK_SHRINK);
     VBox2.pack_start(m_lbl_tab,Gtk::PACK_SHRINK);
     VBox2.pack_start(*m_grid_tab,true,true,100);
     VBox2.pack_start(m_progress_bar, false, false);
     VBox3.pack_start(m_lbl_words,Gtk::PACK_SHRINK);
     VBox3.pack_start(*m_grid_words, Gtk::PACK_SHRINK);
     HBox1.pack_start(VBox2,Gtk::PACK_EXPAND_WIDGET,20);
     HBox1.pack_start(VBox3, Gtk::PACK_EXPAND_WIDGET,20);
     VBox1.pack_start(HBox1);
     add(sc_window);
     sc_window.add(VBox1);
     resize(300, 100);
     show_all_children();
     m_lbl_words.hide();
     m_lbl_tab.hide();
     m_progress_bar.set_visible(false);
}

Word_Search::~Word_Search()
{
     on_delete_grid();
     if (m_grid_tab != nullptr)
     {
          delete m_grid_tab;
     }
     if (m_grid_words!= nullptr)
     {
          delete m_grid_words;
     }
     if (m_dlg_open!=nullptr)
     {
          delete m_dlg_open;
     }
     if (m_vec_color!=nullptr)
     {
          delete m_vec_color;
     }
     if (m_dlg_ocr!=nullptr)
     {
          delete_dlg_ocr();
     }
}

void Word_Search::on_Show()
{
     m_default_cursor=get_window()->get_cursor();
}

void Word_Search::on_Quit()
{
     hide();
     close();
}

void Word_Search::on_options(void)
{
     Dialog_Options dlg_options(m_Parameters);
     int answer;
     answer=dlg_options.run();
     if (answer==Gtk::RESPONSE_OK)
     {
          m_Parameters=dlg_options.Get_Options();
          save_file_parameters();
     }
     m_dlg_new.hide();
}

void Word_Search::on_save_as(void)
{
     int result(0);
     if (m_dlg_open==nullptr)
     {
          m_dlg_open=new Gtk::FileChooserDialog(_("Save a Word Search file"), Gtk::FILE_CHOOSER_ACTION_SAVE);
          m_dlg_open->set_transient_for(*this);
          m_dlg_open->add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
          m_dlg_open->add_button(Gtk::Stock::SAVE, Gtk::RESPONSE_OK);
     }
     if (m_dlg_open!=nullptr)
     {
          result=m_dlg_open->run();
          m_dlg_open->hide();
          if ((result==Gtk::RESPONSE_OK) && (m_dlg_open->get_filename()!=""))
          {
	       m_sz_file_directory=m_dlg_open->get_filename();
	       if (m_check_menu_item_create.get_active())
	       {
		    save<Entry_Letter, Gtk::Entry>(m_sz_file_directory);
	       }
	       else
	       {
		    save<DrawingArea_Letter, Gtk::Label>(m_sz_file_directory);
	       }
	       
	  }
	  delete m_dlg_open;
	  m_dlg_open=nullptr;
     }
}

void Word_Search::on_save(void)
{
     if (m_check_menu_item_create.get_active())
     {
	  save<Entry_Letter, Gtk::Entry>(m_sz_file_directory);
     }
     else
     {
	  save<DrawingArea_Letter, Gtk::Label>(m_sz_file_directory);
     }
}

template <typename Letter, typename Word>
void Word_Search::save(std::string filename)
{
     std::fstream file;
     file.open(filename,std::ios_base::out);
     if (file.is_open())
     {
	  Letter *entry;
	  Word *e_word;
	  for (size_t i=0; i<new_lines_count;i++)
	  {
	       for(size_t j=0; j<new_columns_count; j++)
	       {
		    Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(j, i);
		    entry=(Letter*)get_letter_widget<Letter>(event);
		    if (entry!=nullptr)
		    {
			 file<<entry->get_text();
		    }
		    else
		    {
			 std::ostringstream ostr;
			 ostr<<_("The widget at line : ")<<i<<_(" and column : ")<<j<<_(" don't exist");
			 Gtk::MessageDialog Dialog(ostr.str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
			 Dialog.set_title(_("Widget problem"));
			 Dialog.run();
		    }
	       }
	       file<<std::endl;
	  }
	  file<<std::endl;
	  for (size_t i=0;i<new_words_count;i++)
	  {
	       e_word=(Word*)m_grid_words->get_child_at(0, i);
	       file<<e_word->get_text();
	       file<<std::endl;
	  }
	  file.close();
     }
}

std::vector<std::string> Word_Search::process_words_ocr(char *words)
{
     std::vector<std::string> ret;
     std::string sz_word_grid(words), word;
     std::istringstream istr(sz_word_grid);
     while(std::getline(istr,word))
     {
          if (word!="")
          {
               ret.push_back(word);
          }
     }
     return ret;
}

std::vector<std::string> Word_Search::process_grid_ocr(char *letters)
{
     std::vector<std::string> ret;
     std::string sz_letters_grid(letters), line;
     std::istringstream istr(sz_letters_grid);
     while(std::getline(istr,line))
     {
          if (line!="")
          {
               ret.push_back(line);
          }
     }
     return ret;
}

size_t Word_Search::find_column_width(std::vector<std::string> grid)
{
     size_t ret(0), column_width(0);
     std::map<size_t,size_t> map_grid;
     std::map<size_t, size_t>::iterator first;
     std::pair<size_t, size_t> last;
     for (size_t i=0;i<grid.size();i++)
     {
          std::map<size_t, size_t>::iterator itr;
          itr=map_grid.find(grid.at(i).size());
          if (itr==map_grid.end())
          {
               map_grid.insert(std::pair<size_t, size_t>(grid.at(i).size(),1));
          }
          else
          {
               itr->second++;
          }
     }
     first=map_grid.begin();
     last=*map_grid.rbegin();
     do
     {
          if (first->second > column_width)
          {
               column_width=first->second;
               ret=first->first;
          }
     } while (map_grid.value_comp()(*first++,last));
     return ret;
}

void Word_Search::process_ocr(void)
{
     tesseract::TessBaseAPI *api=new tesseract::TessBaseAPI();
     int success;
     success=api->Init(NULL,m_Parameters.language.c_str());
     if (success!=0)
     {
          Gtk::MessageDialog message(_("OCR initialisation fail"),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
     }
     else
     {
          char *output_text_grid, *output_text_words;
	  m_lines.clear();
	  m_words.clear();
          api->SetVariable("tessedit_char_whitelist", m_Parameters.whitelist.c_str());
          api->SetVariable("tessedit_char_blacklist", m_Parameters.blacklist.c_str());
          Pix *image_grid_letters=pixRead(m_dlg_ocr->Get_path_grid_letters().c_str());
          Pix *image_grid_words=pixRead(m_dlg_ocr->Get_path_words().c_str());
          api->SetImage(image_grid_letters);
          output_text_grid=api->GetUTF8Text();
          std::string sz_text_grid(output_text_grid);
          api->SetImage(image_grid_words);
          output_text_words=api->GetUTF8Text();
          m_words=process_words_ocr(output_text_words);
          m_lines=process_grid_ocr(output_text_grid);
          save_old_variables();
	  on_delete_grid();
          new_columns_count=find_column_width(m_lines);
          new_lines_count=m_lines.size();
          new_words_count=m_words.size();
	  if (m_check_menu_item_create.get_active())
	  {
	       create_grid<Entry_Letter, Gtk::Entry>();
	       populate_grid<Entry_Letter>(m_lines);
	       populate_words<Gtk::Entry>(m_words);
	  }
	  else
	  {
	       create_grid<DrawingArea_Letter, Gtk::Label>();
	       populate_grid<DrawingArea_Letter>(m_lines);
	       populate_words<Gtk::Label>(m_words);
	  }
          check_ocr_words(m_words);
          check_ocr_grid(m_lines);
          api->End();
          delete api;
          delete [] output_text_grid;
          delete [] output_text_words;
          pixDestroy(&image_grid_letters);
          pixDestroy(&image_grid_words);
     }
}

void Word_Search::check_ocr_words(std::vector<std::string> words)
{
     std::vector<size_t> words_nok;
     for (size_t i=0; i<words.size();i++)
     {
	  if (words.at(i).find(" ")!=std::string::npos)
	  {
	       words_nok.push_back(i+1);
	  }
     }
     if (words_nok.size())
     {
	  std::string text(_("This words are not correct : "));
	  for (size_t i=0; i<words_nok.size();i++)
	  {
	       std::ostringstream ostr;
	       ostr<<i;
	       if (i<words_nok.size()-1)
	       {
		    text+=(ostr.str()+", ");
	       }
	       else
	       {
		    text+=ostr.str();
	       }
	  }
	  Gtk::MessageDialog message(text,false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
	  message.run();
     }
}

void Word_Search::check_ocr_grid(std::vector<std::string> grid)
{
     std::vector<size_t> lines_nok;
     for (size_t i=0; i<grid.size();i++)
     {
	  if (grid.at(i).size()!=new_columns_count)
	  {
	       lines_nok.push_back(i+1);
	  }
     }
     if (lines_nok.size())
     {
	  std::string text(_("This lines are not correct : "));
	  for (size_t i=0; i<lines_nok.size();i++)
	  {
	       std::ostringstream ostr;
	       ostr<<lines_nok.at(i);
	       if (i<lines_nok.size()-1)
	       {
		    text+=(ostr.str()+", ");
	       }
	       else
	       {
		    text+=ostr.str();
	       }
	  }
	  Gtk::MessageDialog message(text,false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
	  message.run();
     }
}

void Word_Search::on_open_ocr_files(void)
{
     if (m_dlg_ocr==nullptr)
     {
	  m_dlg_ocr=new Dialog_OCR;
     }
     if (m_dlg_ocr!=nullptr)
     {
	  int answer;
	  answer=m_dlg_ocr->run();
	  if (answer==Gtk::RESPONSE_OK)
	  {
	       process_ocr();
	  }
	  m_dlg_ocr->hide();
     }
     delete_dlg_ocr();
}

void Word_Search::delete_dlg_ocr(void)
{
     if (m_dlg_ocr!=nullptr)
     {
	  delete m_dlg_ocr;
	  m_dlg_ocr=nullptr;
     }
}

void Word_Search::on_open_text_files(void)
{
     int result(0);
     if (m_dlg_open==nullptr)
     {
	  m_dlg_open=new Gtk::FileChooserDialog(_("Open a Word Search file"), Gtk::FILE_CHOOSER_ACTION_OPEN);
     }
     if (m_dlg_open!=nullptr)
     {
	  m_dlg_open->set_transient_for(*this);
	  m_dlg_open->add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	  m_dlg_open->add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
	  if (m_sz_file_directory!="")
	  {
	       m_dlg_open->set_current_folder(m_sz_file_directory);
	  }
	  else
	  {
	       m_dlg_open->set_current_folder(m_sz_share+"/Puzzles");
	  }
	  result=m_dlg_open->run();
	  m_dlg_open->hide();
	  if ((result==Gtk::RESPONSE_OK) && (m_dlg_open->get_filename()!=""))
	  {
	       std::fstream file;
	       file.open(m_dlg_open->get_filename(),std::ios_base::in);
	       if (file.is_open())
	       {
		    std::string line;
		    m_sz_file_directory=m_dlg_open->get_current_folder();
		    save_old_variables();
		    on_delete_grid();
		    m_lines.clear();
		    m_words.clear();
		    while ((std::getline(file,line)) && (line!=""))
		    {
			 if (line!="")
			 {
			      m_lines.push_back(line);
			      if (new_columns_count==0)
			      {
				   new_columns_count=line.size();
			      }
			 }
		    }
		    new_lines_count=m_lines.size();
		    while ((std::getline(file,line)) && (!file.eof()))
		    {
			 m_words.push_back(line);
		    }
		    new_words_count=m_words.size();
		    file.close();
		    if (m_check_menu_item_create.get_active())
		    {
			 create_grid<Entry_Letter, Gtk::Entry>();
			 populate_grid<Entry_Letter>(m_lines);
			 populate_words<Gtk::Entry>(m_words);
		    }
		    else
		    {
			 create_grid<DrawingArea_Letter, Gtk::Label>();
			 populate_grid<DrawingArea_Letter>(m_lines);
			 populate_words<Gtk::Label>(m_words);
		    }
	       }
	  }
	  delete m_dlg_open;
	  m_dlg_open=nullptr;
     }
}

template<typename Letter>
void Word_Search::populate_grid(std::vector<std::string> grid)
{
     for(size_t i=0; i<new_lines_count;i++)
     {
	  Glib::ustring line=grid.at(i);
	  for(size_t j=0; j<new_columns_count;j++)
	  {
	       Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(j, i);
	       Letter *entry=(Letter*)get_letter_widget<Letter>(event);
	       if ((j<line.size()) && (entry!=nullptr))
	       {
		    Glib::ustring carac;
		    carac=line.at(j);
		    entry->set_text(carac);
	       }
	  }
     }
}

template<typename Word>
void Word_Search::populate_words(std::vector<std::string> words)
{
     for (size_t i=0; i<new_words_count;i++)
     {
	  Word *entry=(Word*)m_grid_words->get_child_at(0, i);
	  entry->set_text(words.at(i));
	  if (!m_check_menu_item_create.get_active())
	  {
	       int *data=new int(0);
	       entry->set_data("Found",data);
	  }
     }
}

void Word_Search::on_delete_grid(bool reset_vec_colors)
{
     if (m_check_menu_item_create.get_active())
     {
	  delete_grid<Entry_Letter>(reset_vec_colors);
     }
     else
     {
	  delete_grid<DrawingArea_Letter>(reset_vec_colors);
     }
}

template<typename Letter>
void Word_Search::delete_grid(bool reset_vec_colors)
{
     for (size_t i=0;i<old_lines_count;i++)
     {
	  for (size_t j=0;j<old_columns_count;j++)
	  {
	       Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(j,i);
	       delete get_letter_widget<Letter>(event);
	       delete m_grid_tab->get_child_at(j, i);
	  }
     }
     for (size_t i=0; i<old_words_count;i++)
     {
	  delete m_grid_words->get_child_at(0, i);
     }
     if (reset_vec_colors)
     {
	  delete m_vec_color;
	  m_vec_color=nullptr;
     }
}

void Word_Search::add_signals_letter(Gtk::Entry *entry, size_t i, size_t j)
{
     entry->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Word_Search::on_entry_tabs_changed),i,j));
}

void Word_Search::add_signals_letter(Entry_Letter *entry, size_t i, size_t j)
{
     entry->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Word_Search::on_entry_tabs_changed),i,j));
}

void Word_Search::add_signals_letter(Gtk::EventBox *event_label, size_t i, size_t j)
{
     if (!m_check_menu_item_create.get_active())
     {
	  event_label->signal_button_press_event().connect(sigc::bind(sigc::mem_fun(*this,&Word_Search::on_event_label_button_pressed),i,j));
	  event_label->signal_button_release_event().connect(sigc::bind(sigc::mem_fun(*this,&Word_Search::on_event_label_button_released),i,j));
	  event_label->signal_enter_notify_event().connect(sigc::bind(sigc::mem_fun(*this, &Word_Search::on_event_label_notify_event),i,j));
     }
}

void Word_Search::add_signals_word(Gtk::Entry *entry, size_t i)
{
     entry->signal_changed().connect(sigc::bind(sigc::mem_fun(*this, &Word_Search::on_entry_words_changed),i));
}

void Word_Search::add_signals_word(Gtk::Label *label, size_t i)
{
  
}

template <typename NumberType>
NumberType Word_Search::get_random_number(NumberType start, NumberType end)
{
     NumberType ret;
     std::random_device rd;
     std::mt19937 mt(rd());
     std::uniform_int_distribution<size_t> dist_color(start,end);
     ret=dist_color(mt);
     return ret;
}

template<typename Letter, typename Word>
void Word_Search::create_grid(bool create_colors)
{
     Letter *entry;
     if (m_vec_color==nullptr)
     {
	  m_vec_color=new std::vector<std::string>;
     }
     for (size_t i=0; i<new_lines_count;i++)
     {
	  for (size_t j=0; j<new_columns_count;j++)
	  {
	       Gtk::EventBox *event=new Gtk::EventBox;
	       entry=new Letter;
	       add_signals_letter(event, i, j);
	       m_grid_tab->attach(*event, j, i);
	       event->add(*entry);
	       if (m_check_menu_item_create.get_active())
	       {
		    add_signals_letter((Gtk::Entry*)entry,i,j);
	       }
	  }
     }
     for (size_t i=0; i<new_words_count;i++)
     {
	  Glib::RefPtr<Gtk::CssProvider> css=Gtk::CssProvider::create();
	  Glib::RefPtr<Gtk::StyleContext> style;
	  std::ostringstream ostr_theme, ostr_color;
	  Word *e_word;
	  size_t color;
	  e_word=new Word;
	  style=e_word->get_style_context();
	  if (m_check_menu_item_create.get_active())
	  {
	       ostr_theme<<"entry {color: #";
	  }
	  else
	  {
	       Gtk::Label *label=(Gtk::Label*)e_word;
	       ostr_theme<<"label {color: #";
	       label->set_use_markup(true);
	  }
	  if (create_colors)
	  {
	       color=get_random_number(0, 16777215);
	       ostr_color<<std::setfill('0')<<std::setw(6)<<std::hex<<color;
	       ostr_theme<<ostr_color.str();
	       m_vec_color->push_back(ostr_color.str());
	  }
	  else
	  {
	       ostr_theme<<m_vec_color->at(i);
	  }
	  ostr_theme<<";}";
	  css->load_from_data(ostr_theme.str());
	  style->add_provider(css, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	  add_signals_word(e_word, i);
	  m_grid_words->attach(*e_word, 0, i);
     }
     maximize();
     show_all_children();
     m_progress_bar.set_visible(false);
}

void Word_Search::save_old_variables(bool reset_new_variables)
{
     old_columns_count=new_columns_count;
     old_lines_count=new_lines_count;
     old_words_count=new_words_count;
     if (reset_new_variables)
     {
	  new_columns_count=0;
	  new_lines_count=0;
	  new_words_count=0;
     }
}

std::string Word_Search::get_template_line_direction(const size_t row, const size_t column, const int x_direction, const int y_direction, const size_t length)
{
     std::string ret;
     int cur_row(row), cur_column(column);
     while ((cur_row>=0) && (cur_column>=0) && (cur_row<new_lines_count) && (cur_column<new_columns_count) && (ret.size()<length))
     {
	  std::string car;
	  car=m_lines.at(cur_row).at(cur_column);
	  if (car==" ")
	  {
	       ret+=" ";
	  }
	  else
	  {
	       ret+=car;
	  }
	  cur_row+=y_direction;
	  cur_column+=x_direction;
     }
     return ret;
}

std::string Word_Search::get_regex(std::string template_word)
{
     std::string ret;
     for (size_t i=0;i<template_word.size();i++)
     {
	  if (template_word.at(i)==' ')
	  {
	       ret+="[a-z]";
	  }
	  else
	  {
	       ret+=template_word.at(i);
	  }
     }
     return ret;
}

void Word_Search::set_word_in_tab(const std::string word, const size_t initial_row, const size_t initial_column, const int x_direction, const int y_direction)
{
     for (size_t i=0; i<word.size();i++)
     {
	  m_lines.at(initial_row+y_direction*i).at(initial_column+x_direction*i)=word.at(i);
     }
}

size_t Word_Search::get_max_letters(const size_t initial_row, const size_t initial_column, const int x_direction, const int y_direction)
{
     size_t ret(0);
     int   x(initial_column), y(initial_row);
     do
     {
	  ret++;
	  x+=x_direction;
	  y+=y_direction;
     } while ((x>=0) && (y>=0) && (x<new_columns_count) && (y<new_lines_count));
     return ret;
}

bool Word_Search::is_word_in_tab(const std::string word)
{
     bool ret(false);
     for( size_t i=0; (i<m_words.size()&& (!ret));i++)
     {
	  if (word==m_words[i])
	  {
	       ret=true;
	  }
     }
     return ret;
}

void Word_Search::fill_grid(void)
{
     for (size_t i=0; i<m_lines.size();i++)
     {
	  for (size_t j=0; j<m_lines.at(i).size();j++)
	  {
	       if (m_lines.at(i).at(j)==' ')
	       {
		    char car;
		    car=get_random_number(char('A'), char('Z'));
		    m_lines.at(i).at(j)=car;
	       }
	  }
     }
}

void Word_Search::uppercase_letters_words(void)
{
     for (size_t i=0; i<m_lines.size();i++)
     {
	  std::transform(m_lines.at(i).begin(),m_lines.at(i).end(),m_lines.at(i).begin(),::toupper);
     }
     for (size_t i=0; i<m_words.size();i++)
     {
	  std::transform(m_words.at(i).begin(),m_words.at(i).end(),m_words.at(i).begin(),::toupper);
     }
}

void Word_Search::on_generate(void)
{
//Generate the word with direction, position and max_length of the word
     m_lines.clear();
     m_words.clear();
     for (size_t i=0; i<new_lines_count;i++)
     {
	  std::string tmp(new_columns_count,' ');
	  m_lines.push_back(tmp);
     }
     m_progress_bar.show();
     for (size_t i=0; i<new_words_count;i++)
     {
	  size_t  initial_position_x, initial_position_y, max_length(0);
	  int direction_x, direction_y;
	  std::string new_word;
	  do
	  {
	       std::string template_word;
	       std::vector<std::string> vec_words;
	       std::regex regex;
	       do
	       {
		    size_t length;
		    initial_position_x=get_random_number(size_t(1), new_columns_count-1);
		    initial_position_y=get_random_number(size_t(1), new_lines_count-1);
		    direction_x=get_random_number(-1, 1);
		    do {
			 direction_y=get_random_number(-1, 1);
		    } while ((direction_x==0) && (direction_y==0));
		    length=get_max_letters(initial_position_y, initial_position_x, direction_x, direction_y);
		    if (length > 3)
		    {
			 max_length=get_random_number(size_t(3), get_max_letters(initial_position_y, initial_position_x, direction_x, direction_y));
		    }
		    else
		    {
			 max_length=0;
		    }
	       }while (max_length<=2);
	       template_word=get_template_line_direction(initial_position_y, initial_position_x, direction_x, direction_y, max_length);
	       regex=get_regex(template_word);
	       vec_words=dict.get_words(regex);
	       if (vec_words.size()>1)
	       {
		    size_t index_word;
		    index_word=get_random_number(size_t(0), vec_words.size()-1);
		    new_word=vec_words.at(index_word);
	       }
	       else if (vec_words.size()==1)
	       {
		    new_word=vec_words.at(0);
	       }
	  } while ((new_word=="") && (!is_word_in_tab(new_word)));
	  set_word_in_tab(new_word, initial_position_y, initial_position_x, direction_x, direction_y);
	  m_words.push_back(new_word);
	  m_progress_bar.set_fraction((double)((double)i/(double)(new_words_count-1)));
	  while ( Gtk::Main::events_pending() ) Gtk::Main::iteration();
     }
     uppercase_letters_words();
     fill_grid();
     populate_grid<Entry_Letter>(m_lines);
     populate_words<Gtk::Entry>(m_words);
     m_progress_bar.hide();
}

void Word_Search::on_New()
{
     int answer;
     m_check_menu_item_create.set_active();
     save_old_variables();
     answer=m_dlg_new.run();
     if (answer==Gtk::RESPONSE_OK)
     {
	  m_sz_file_directory="";
	  on_delete_grid();
	  new_lines_count=m_dlg_new.Get_Lines_Count();
	  new_columns_count=m_dlg_new.Get_Columns_Count();
	  new_words_count=m_dlg_new.Get_Words_Count();
	  create_grid<Entry_Letter,Gtk::Entry>();
     }
     m_dlg_new.hide();
}

void Word_Search::on_entry_words_changed(size_t row)
{
     Gtk::Entry *entry;
     Glib::ustring carac;
     entry=(Gtk::Entry*)m_grid_words->get_child_at(0, row);
     entry->set_text(entry->get_text().uppercase());
     if (entry->get_text_length()>0)
     {
	  carac=entry->get_text().substr(entry->get_text_length()-1,1);
     }
     if (m_Parameters.ascii)
     {
	  if ((carac<"A") || (carac>"Z"))
	  {
	       entry->set_text(entry->get_text().substr(0,entry->get_text_length()-1));
	  }
     }
     m_words.at(row)=entry->get_text();
}

void Word_Search::on_entry_tabs_changed(size_t row, size_t column)
{
     Entry_Letter *entry;
     Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(column, row);
     entry=(Entry_Letter*)get_letter_widget<Entry_Letter>(event);
     entry->set_text(entry->get_text().uppercase());
     if (entry->get_text()!="")
     {
	  if (m_Parameters.ascii)
	  {
	       if ((entry->get_text()<"A") || (entry->get_text()>"Z"))
	       {
		    entry->set_text("");
		    Gtk::MessageDialog message(_("The character inserted is not a letter"),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
		    message.run();
	       }
	  }
	  m_lines.at(row).replace(column,1,entry->get_text());
     }
}

bool Word_Search::on_event_label_button_pressed(GdkEventButton *button, size_t row, size_t column)
{
     if (m_manual_solve)
     {
	  m_word_choice=true;
	  m_row_press_button=row;
	  m_column_press_button=column;
     }
     return false;
}

bool Word_Search::on_event_label_button_released(GdkEventButton *button, size_t row, size_t column)
{
     return false;
}

bool Word_Search::on_event_label_notify_event(GdkEventCrossing *crossing_event, size_t row, size_t column)
{
     if ((m_manual_solve) && (m_word_choice))
     {
	  word_in_tab word_tab;
	  int index;
	  m_word_choice=false;
	  m_row_release_button=row;
	  m_column_release_button=column;
	  word_tab=get_word(m_row_press_button, m_column_press_button, m_row_release_button, m_column_release_button);
	  index=get_index_word(word_tab.word)-1;
	  if ((index>=0) && (index<new_words_count))
	  {
	       Gtk::Label *label_word=(Gtk::Label*)m_grid_words->get_child_at(0, index);
	       int *data=(int*)label_word->get_data("Found");
	       if (*data==0)
	       {
		    std::string label_text("<s>");
		    set_areadrawing_letter_color_directon_line(m_row_press_button, m_column_press_button, word_tab.x_direction, word_tab.y_direction,label_word->get_text().size(), m_grid_tab, m_vec_color->at(index));
		    label_text+=(label_word->get_text()+"</s>");
		    label_word->set_markup(label_text);
		    *data=1;
		    label_word->set_data("Found",data);
		    queue_draw();
	       }
	  }
     }
     return  false;
}

int Word_Search::get_index_word(std::string word)
{
     int ret(0);
     bool find(false);
     while ((!find) && (ret<new_words_count))
     {
	  Gtk::Label *label;
	  label=(Gtk::Label*)m_grid_words->get_child_at(0, ret);
	  if (word==label->get_text())
	  {
	       find=true;
	  }
	  ret++;
     }
     if (!find)
     {
	  ret=-1;
     }
     return ret;
}

word_in_tab Word_Search::get_word(size_t row_pressed, size_t column_pressed, size_t row_released, size_t column_released)
{
     word_in_tab ret;
     size_t column, line;
     double slop;
     slop=double((row_pressed)-double(row_released))/(double(column_pressed)-double(column_released));
     if ((std::isinf(slop)) || (abs(slop)==1) || (slop==0))
     {
	  ret.y_direction=get_direction(row_pressed, row_released);
	  ret.x_direction=get_direction(column_pressed, column_released);
	  if (!((ret.x_direction==0) && (ret.y_direction==0)))
	  {
	       Gtk::EventBox *event;
	       line=row_pressed;
	       column=column_pressed;
	       while((line!=row_released) || (column!=column_released))
	       {
		    event=(Gtk::EventBox*)m_grid_tab->get_child_at(column, line);
		    ret.word+=get_letter_widget<DrawingArea_Letter>(event)->get_text();
		    line+=ret.y_direction;
		    column+=ret.x_direction;
	       }
	       event=(Gtk::EventBox*)m_grid_tab->get_child_at(column, line);
	       ret.word+=get_letter_widget<DrawingArea_Letter>(event)->get_text();
	  }
     }
     return ret;
}

int Word_Search::get_direction(size_t begin, size_t end)
{
     int ret;
     if (begin==end)
     {
	  ret=0;
     }
     else if (begin<end)
     {
	  ret=1;
     }
     else
     {
	  ret=-1;
     }
     return ret;
}

void Word_Search::on_Dialog_About()
{
     std::vector<Glib::ustring> people;
     people.push_back("lann");
     m_dlgAbout.add_credit_section(_("Created by"), people);
     m_dlgAbout.set_comments(
	  _("Word Search is a program to solve Word Search grid"));
     m_dlgAbout.set_license_type(Gtk::License::LICENSE_GPL_3_0);
     m_dlgAbout.set_program_name("Word Search");
     m_dlgAbout.set_version("0.6.1.0");
     m_dlgAbout.set_website("https://numeriquement.fr");
     m_dlgAbout.set_translator_credits("lann");
     if (m_sz_share != "") {
	  m_dlgAbout.set_logo(
	       Gdk::Pixbuf::create_from_file(m_sz_share + "/word_search.ico"));
     }
     m_dlgAbout.set_icon(get_icon());
     m_dlgAbout.run();
     m_dlgAbout.hide();
}

bool Word_Search::is_empty_grid_words(void)
{
     bool grid_word_empty;
     if (m_check_menu_item_create.get_active())
     {
	  grid_word_empty=grid_words_empty<Gtk::Entry>();
     }
     else
     {
	  grid_word_empty=grid_words_empty<Gtk::Label>();
     }
     return grid_word_empty;
}

bool Word_Search::is_empty_tab(void)
{
     bool grid_letter_empty;
     if (m_check_menu_item_create.get_active())
     {
	  grid_letter_empty=grid_tab_empty<Entry_Letter>();
     }
     else
     {
	  grid_letter_empty=grid_tab_empty<DrawingArea_Letter>();
     }
     return grid_letter_empty;
}

bool Word_Search::is_full_tab(void)
{
     bool grid_letter_full;
     if (m_check_menu_item_create.get_active())
     {
	  grid_letter_full=grid_tab_full<Entry_Letter>();
     }
     else
     {
	  grid_letter_full=grid_tab_full<DrawingArea_Letter>();
     }
     return grid_letter_full;
}

bool Word_Search::is_full_grid_words(void)
{
     bool grid_word_full;
     if (m_check_menu_item_create.get_active())
     {
	  grid_word_full=grid_words_full<Gtk::Entry>();
     }
     else
     {
	  grid_word_full=grid_words_full<Gtk::Label>();
     }
     return grid_word_full;
}

template <typename Letter>
bool Word_Search::grid_tab_empty(void)
{
     bool ret=true;
     if ((new_lines_count!=0) && (new_columns_count!=0))
     {
	  for (size_t i=0; i<new_lines_count && ret ;i++)
	  {
	       for (size_t j=0;j<new_columns_count && ret;j++)
	       {
		    Letter *entry;
		    Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(j, i);
		    entry=(Letter*)get_letter_widget<Letter>(event);
		    if (entry->get_text()!="")
		    {
			 ret=false;
		    }
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     return ret;
}

template <typename Word>
bool Word_Search::grid_words_empty(void)
{
     bool ret=true;
     if (new_words_count!=0)
     {
	  for (size_t i=0; i<new_words_count && ret; i++)
	  {
	       Word *entry;
	       entry=(Word*)m_grid_words->get_child_at(0, i);
	       if (entry->get_text()!="")
	       {
		    ret=false;
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     return ret;
}

template<typename Letter>
bool Word_Search::grid_tab_full(void)
{
     bool ret=true;
     if ((new_lines_count!=0) && (new_columns_count!=0))
     {
	  for (size_t i=0; i<new_lines_count && ret ;i++)
	  {
	       for (size_t j=0;j<new_columns_count && ret;j++)
	       {
		    Letter *entry;
		    Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(j, i);
		    entry=(Letter*)get_letter_widget<Letter>(event);
		    if (entry->get_text()=="")
		    {
			 ret=false;
		    }
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     return ret;
}

template<typename Word>
bool Word_Search::grid_words_full(void)
{
     bool ret=true;
     if (new_words_count!=0)
     {
	  for (size_t i=0; i<new_words_count && ret; i++)
	  {
	       Word *entry;
	       entry=(Word*)m_grid_words->get_child_at(0, i);
	       if (entry->get_text()=="")
	       {
		    ret=false;
	       }
	  }
     }
     else
     {
	  ret=false;
     }
     return ret;
}

void Word_Search::open_file_parameters(std::string path)
{
     try
     {
	  if (m_key_file.load_from_file(path,Glib::KEY_FILE_KEEP_COMMENTS | Glib::KEY_FILE_KEEP_TRANSLATIONS))
	  {
	       try
	       {
		    m_Parameters.language=m_key_file.get_string("OCR", "language");
		    m_Parameters.whitelist=m_key_file.get_string("OCR", "whitelist");
		    m_Parameters.blacklist=m_key_file.get_string("OCR", "blacklist");
		    m_Parameters.ascii=m_key_file.get_boolean("OCR","ascii");
		    m_Parameters.dictionary_path=m_key_file.get_string("DICT", "path");
		    open_dict();
	       }
	       catch (const Glib::KeyFileError &err)
	       {
		    std::string text(_("Cannot open the key.\nError is : "));
		    text+=err.what();
		    text+_("\nThe default parameters are stored");
		    Gtk::MessageDialog message(text,false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
		    default_file_parameters();
	       }
	  }	  
     }
     catch (const Glib::FileError &err)
     {
	  if (err.code()==Glib::FileError::NO_SUCH_ENTITY)
	  {
	       create_file_parameters();
	  }
	  else
	  {
	       std::string text(_("Cannot open the ini file.\nError is : "));
	       text+=err.what();
	       text+_("\nThe default parameters are stored");
	       Gtk::MessageDialog message(text,false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
	       default_file_parameters();
	  }
     }
}

void Word_Search::open_dict(void)
{
     if (m_Parameters.dictionary_path!="")
     {
	  dict.test_validity(m_Parameters.dictionary_path);
     }
}

void Word_Search::create_file_parameters(void)
{
#ifdef __linux__
     struct stat st;
     if(stat(m_sz_home.c_str(),&st) != 0) //If no directory .word_search
     {
	  if (mkdir(m_sz_home.c_str(),0777) !=0)   //Create this directory
	  {
	       std::string message(_("Cannot create the .word_search directory\n"));
	       message+=_("The software stopped");
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("Problem to create directory"));
	       Dialog.run();
	       on_Quit();
	  }
	  default_file_parameters();
     }
#endif
}

void Word_Search::save_file_parameters(void)
{     
     m_key_file.set_string("OCR", "language",m_Parameters.language);
     m_key_file.set_string("OCR", "whitelist",m_Parameters.whitelist);
     m_key_file.set_string("OCR", "blacklist", m_Parameters.blacklist);
     m_key_file.set_boolean("OCR", "ascii", m_Parameters.ascii);
     m_key_file.set_string("DICT","path",m_Parameters.dictionary_path);
     try
     {
	  m_key_file.save_to_file(m_szparameters_ini);
	  open_dict();
     }
     catch (Glib::FileError &error)
     {
	  std::string text(_("Cannot save to the ini file.\nError is : "));
	  text+=error.what();
	  Gtk::MessageDialog message(text,false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
     }
}

void Word_Search::default_file_parameters(void)
{
     m_Parameters.ascii=true;
     m_Parameters.whitelist="";
     m_Parameters.blacklist="";
     m_Parameters.language="fra";
     m_Parameters.dictionary_path="";
     save_file_parameters();
}

bool Word_Search::on_draw_menu_check_create(const ::Cairo::RefPtr< ::Cairo::Context > &cr)
{
     if ((new_words_count!=0) && (new_columns_count!=0) && (new_lines_count!=0) && (is_full_grid_words()) && (is_full_tab()))
     {
	  m_check_menu_item_create.set_sensitive();
     }
     else
     {
	  m_check_menu_item_create.set_sensitive(false);
	  m_check_menu_item_create.set_active();
     }
     return true;
}

bool Word_Search::on_draw_menu_generate(const ::Cairo::RefPtr< ::Cairo::Context > &cr)
{
     if ((m_check_menu_item_create.get_active()) && (dict.is_valid()) && (is_empty_tab()) && (is_empty_grid_words()) && (new_words_count!=0) && (new_columns_count!=0) && (new_lines_count!=0))
     {
	  m_menu_image_generate.set_sensitive();
     }
     else
     {
	  m_menu_image_generate.set_sensitive(false);
     }
     return true;
}

bool Word_Search::on_Draw_Menu_Solve(const ::Cairo::RefPtr<::Cairo::Context> &cr)
{
     if ((is_full_tab()) && (is_full_grid_words()) && (!m_check_menu_item_create.get_active()))
     {
	  m_menu_image_computing_solve.set_sensitive(true);
	  m_menu_image_manual_solve.set_sensitive(true);
     }
     else
     {
	  m_menu_image_computing_solve.set_sensitive(false);
	  m_menu_image_manual_solve.set_sensitive(false);
     }
     return true;
}

void Word_Search::on_toggled_check_create(void)
{
     if (m_check_menu_item_create.get_active())
     {
	  create_mode();
     }
     else
     {
	  read_mode();
     }
}

void Word_Search::read_mode(void)
{
     change_grid<DrawingArea_Letter, Gtk::Label>();
}

void Word_Search::create_mode(void)
{
     change_grid<Entry_Letter, Gtk::Entry>();
     m_manual_solve=false;
     m_word_choice=false;
}

template <typename Letter, typename Word>
void Word_Search::change_grid(void)
{
     save_old_variables(false);
     on_delete_grid(false);
     create_grid<Letter, Word>(false);
     populate_grid<Letter>(m_lines);
     populate_words<Word>(m_words);
     show_all_children();
     m_progress_bar.set_visible(false);
}

void Word_Search::on_manual_solve(void)
{
     m_manual_solve=true;
}

void Word_Search::on_computing_solve(void)
{
     m_manual_solve=false;
     element_solving element;
     element.tab_grid=m_grid_tab;
     element.words_grid=m_grid_words;
     element.words_count=new_words_count;
     element.column_count=new_columns_count;
     element.row_count=new_lines_count;
     element.vec_sz_color=m_vec_color;
     for (size_t i=0; i<new_lines_count ;i++)
     {
	  for (size_t j=0;j<new_columns_count;j++)
	  {
	       DrawingArea_Letter *draw_letter;
	       Gtk::EventBox *event=(Gtk::EventBox*)m_grid_tab->get_child_at(j, i);
	       draw_letter=(DrawingArea_Letter*)get_letter_widget<DrawingArea_Letter>(event);
	       draw_letter->unset_color();
	  }
     }
     Word_Search_Solve solve(element);
     show_all_children();
}

bool Word_Search::on_draw_menu_save(const ::Cairo::RefPtr< ::Cairo::Context > &cr)
{
     if ((new_columns_count!=0) && (new_lines_count!=0) && (new_words_count!=0))
     {
	  m_menu_image_save_as.set_sensitive();
	  if (m_sz_file_directory!="")
	  {
	       m_menu_image_save.set_sensitive();
	  }
	  else
	  {
	       m_menu_image_save.set_sensitive(false);	       
	  }
     }
     else
     {
	  m_menu_image_save_as.set_sensitive(false);
	  m_menu_image_save.set_sensitive(false);
     }
     return true;
}
