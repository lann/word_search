#ifndef DIALOG_NEW_INCLUDED
#define DIALOG_NEW_INCLUDED
#include "gtkmm/button.h"
#include "gtkmm/dialog.h"
#include "gtkmm/entry.h"
#include "gtkmm/filechooserdialog.h"
#include "gtkmm/label.h"
class Dialog_New : public Gtk::Dialog
{
public:
     Dialog_New();
     ~Dialog_New();
     unsigned int Get_Columns_Count();
     unsigned int Get_Lines_Count();
     unsigned int Get_Words_Count();
protected:
     void entry_changed(void);
private:
     Gtk::Label m_lbl_colomn, m_lbl_line, m_lbl_word;
     Gtk::Entry m_e_column, m_e_line, m_e_word;
     Gtk::Button *bOK, *bCancel;
     unsigned int m_columns_count, m_lines_count, m_words_count;
};
#endif //DIALOG_NEW_INCLUDED
