#include "entry_letter.h"
#include "gdkmm/cursor.h"
#include "gdkmm/pixbuf.h"
#include "glibmm/keyfile.h"
#include "glibmm/refptr.h"
#include "gtkmm/checkmenuitem.h"
#include "gtkmm/dialog.h"
#include "gtkmm/entry.h"
#include "gtkmm/eventbox.h"
#include "gtkmm/hvbox.h"
#include <cstddef>
#include <gtkmm.h>
#include <string>
#include <vector>
#include "dialog_new.h"
#include "dialog_ocr.h"
#include "gtkmm/label.h"
#include "gtkmm/progressbar.h"
#include "word_search_solve.h"
#include "gtkmm/scrolledwindow.h"
#include "gtkmm/window.h"
#include "dialog_options.h"
#include "drawingarea_letter.h"
#include "dictionary.h"
#ifndef WORD_SEARCH_H_INCLUDED
#define WORD_SEARCH_H_INCLUDED

typedef struct
{
     bool ascii;
     std::string whitelist;
     std::string blacklist;
     std::string language;
     std::string dictionary_path;
} parameters;

typedef struct
{
     std::string word;
     int x_direction;
     int y_direction;
} word_in_tab;

class Word_Search : public Gtk::Window
{
public:
     Word_Search();
     ~Word_Search();
protected:
     virtual void on_Show(void);
     virtual void on_Quit();
     virtual void on_New();
     virtual void on_Dialog_About(void);
     virtual void on_computing_solve(void);
     virtual void on_manual_solve(void);
     virtual void on_generate(void);
     virtual void on_toggled_check_create(void);
     virtual void on_open_text_files(void);
     virtual void on_open_ocr_files(void);
     virtual void on_save_as();
     virtual void on_save();
     virtual void on_options();
     virtual bool on_draw_menu_save(const ::Cairo::RefPtr< ::Cairo::Context > &cr);
     virtual bool on_Draw_Menu_Solve(const ::Cairo::RefPtr< ::Cairo::Context>& cr);
     virtual bool on_draw_menu_check_create(const ::Cairo::RefPtr< ::Cairo::Context > &cr);
     virtual bool on_draw_menu_generate(const ::Cairo::RefPtr< ::Cairo::Context > &cr);
     virtual bool on_event_label_button_pressed(GdkEventButton *button, size_t row, size_t column);
     virtual bool on_event_label_button_released(GdkEventButton *button, size_t row, size_t column);
     virtual bool on_event_label_notify_event(GdkEventCrossing *crossing_event, size_t row, size_t column);
private:
     void on_entry_tabs_changed(size_t row, size_t column);
     void on_entry_words_changed(size_t row);
     void create_file_parameters(void);
     void save_file_parameters(void);
     void open_file_parameters(std::string path);
     void default_file_parameters(void);
     void open_dict(void);
     template<typename Letter> void delete_grid(bool reset_vec_colors=true);
     void on_delete_grid(bool reset_vec_colors=true);
     template<typename Letter> bool grid_tab_full(void);
     template<typename Word> bool grid_words_full(void);
     template<typename Letter> bool grid_tab_empty(void);
     template<typename Word> bool grid_words_empty(void);
     bool is_full_tab(void);
     bool is_full_grid_words(void);
     bool is_empty_tab(void);
     bool is_empty_grid_words(void);
     void save_old_variables(bool reset_new_variables=true);
     void delete_dlg_ocr(void);
     void process_ocr(void);
     size_t find_column_width(std::vector<std::string> grid);
     template<typename Letter> void populate_grid(std::vector<std::string> grid);
     template<typename Word> void populate_words(std::vector<std::string> words);
     void check_ocr_words(std::vector<std::string> words);
     void check_ocr_grid(std::vector<std::string> grid);
     void read_mode(void);
     void create_mode(void);
     template<typename Letter, typename Word> void change_grid(void);
     template<typename Letter, typename Word> void create_grid(bool create_colors=true);
     template<typename Letter> void add_signals_letter(Letter *object, size_t i, size_t j);
     void add_signals_letter(Gtk::Entry *entry, size_t i, size_t j);
     void add_signals_letter(Entry_Letter *entry, size_t i, size_t j);
     void add_signals_letter(Gtk::EventBox *event_label, size_t i, size_t j);
     void add_signals_word(Gtk::Entry *entry, size_t i);
     void add_signals_word(Gtk::Label *label, size_t i);
     word_in_tab get_word(size_t row_pressed, size_t column_pressed, size_t row_released,size_t column_released);
     int get_direction(size_t begin, size_t end);
     int get_index_word(std::string word);
     template<typename NumberType> NumberType get_random_number(NumberType start, NumberType end);
     template<typename Letter,typename Word> void save(std::string filename);
     std::vector<std::string> process_words_ocr(char *words);
     std::vector<std::string> process_grid_ocr(char *letters);
     std::string get_template_line_direction(const size_t row, const size_t column, const int x_direction, const int y_direction, const size_t length);
     void set_word_in_tab(const std::string word, const size_t initial_row, const size_t initial_column, const int x_direction, const int y_direction);
     size_t get_max_letters(const size_t initial_row, const size_t initial_column, const int x_direction, const int y_direction);
     std::string get_regex(std::string template_word);
     bool is_word_in_tab(const std::string word);
     void fill_grid(void);
     void uppercase_letters_words(void);
     Glib::KeyFile m_key_file;
     Gtk::ProgressBar m_progress_bar;
     Gtk::Grid *m_grid_tab, *m_grid_words;
     Gtk::Image m_pic_solve, m_image_manual_solve;
     Glib::RefPtr<Gdk::Cursor> m_default_cursor;
     Gtk::MenuBar m_menubar;
     Gtk::Menu m_menu_file, m_menu_file_open, m_menu_help, m_menu_word_search, m_menu_mode;
     Gtk::MenuItem m_menuitem_file, m_menuitem_help, m_menuitem_word_search, m_menuitem_mode;
     Gtk::CheckMenuItem m_check_menu_item_create;
     Gtk::ImageMenuItem m_menu_image_quit, m_menu_image_new, m_menu_item_open, m_menu_image_open_text, m_menu_image_open_ocr,m_menu_image_save_as, m_menu_image_save, m_menu_image_about, m_menu_image_computing_solve,m_menu_image_manual_solve, m_menu_image_generate, m_menu_image_options;
     Gtk::Label m_lbl_tab, m_lbl_words;
     Gtk::VBox VBox1, VBox2, VBox3;
     Gtk::HBox HBox1;
     Gtk::ScrolledWindow sc_window;
     Gtk::AboutDialog m_dlgAbout;
     Gtk::FileChooserDialog *m_dlg_open;
     Dialog_New m_dlg_new;
     Dialog_OCR *m_dlg_ocr;
     std::string m_sz_share, m_szparameters_ini, m_sz_home, m_sz_file_directory;
     std::vector<std::string> *m_vec_color, m_lines, m_words;
     size_t old_columns_count, old_lines_count, old_words_count, new_columns_count, new_lines_count, new_words_count;
     parameters m_Parameters;
     bool m_manual_solve, m_word_choice;
     size_t m_row_press_button,m_column_press_button, m_row_release_button, m_column_release_button;
     dictionary dict;
};
#endif
