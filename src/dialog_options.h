#ifndef DIALOG_OPTIONS_H
#define DIALOG_OPTIONS_H
#include "gtkmm/button.h"
#include "gtkmm/checkbutton.h"
#include "gtkmm/entry.h"
#include "gtkmm/filechooserbutton.h"
#include "gtkmm/filechooserdialog.h"
#include "gtkmm/label.h"
#include "word_search.h"

class Dialog_Options : public Gtk::Dialog
{
public:
     Dialog_Options();
     Dialog_Options(parameters options);
     virtual ~Dialog_Options();
     parameters Get_Options(void);
protected:
     void on_check_button_ascii(void);
     void on_OK(void);
private:
     void Init(void);
     parameters m_options;
     Gtk::Button *OK, *Cancel;
     Gtk::Label lbl_language, lbl_whitelist, lbl_blacklist, lbl_path_dictionary;
     Gtk::Entry e_language, e_whitelist, e_blacklist;
     Gtk::FileChooserButton m_btn_dictionary;
     Gtk::CheckButton chk_ascii;
};


#endif // DIALOG_OPTIONS_H
