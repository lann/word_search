#ifndef DICTIONARY
#define DICTIONARY
#include <cstddef>
#include <string>
#include <vector>
#include <regex>

class dictionary
{
public:
     dictionary();
     ~dictionary();
     void test_validity(std::string path);
     bool is_valid();
     size_t get_number_words(void);
     std::vector<std::string> get_words(const std::regex regex);
private:
     std::vector<std::string> dict;
     bool m_valid;
};

#endif //DICTIONARY
