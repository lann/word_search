#ifndef DIALOG_OCR_H
#define DIALOG_OCR_H
#include "gtkmm/button.h"
#include "gtkmm/dialog.h"
#include "gtkmm/entry.h"
#include "gtkmm/filechooserdialog.h"
#include "gtkmm/hvbox.h"
#include "gtkmm/label.h"
#include <string>

class Dialog_OCR : public Gtk::Dialog
{
public:
     Dialog_OCR();
     ~Dialog_OCR();
     std::string Get_path_grid_letters(void);
     std::string Get_path_words(void);
protected:
     void on_entries_changed(void);
     void on_open_grid_file(void);
     void on_open_word_file(void);
private:
     int Display_File_Chooser(std::string title);
     void delete_dlg_ocr(void);
     Gtk::Label lbl_ocr_letters, lbl_ocr_words;
     Gtk::Entry e_ocr_letters, e_ocr_words;
     Gtk::Button b_ocr_letters, b_ocr_words, *b_OK, *b_Cancel;
     Gtk::HBox hb_ocr_letters, hb_ocr_words;
     Gtk::FileChooserDialog *m_dlg_open_ocr_file;
};
#endif //DIALOG_OCR_H
