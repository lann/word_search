#include "dialog_ocr.h"
#include "gtkmm/box.h"
#include "gtkmm/dialog.h"
#include "gtkmm/filechooser.h"
#include "gtkmm/filechooserdialog.h"
#include "gtkmm/stock.h"
#include "sigc++/functors/mem_fun.h"
#include <libintl.h>
#include <string>
#define _(String) gettext (String)

Dialog_OCR::Dialog_OCR() : lbl_ocr_letters(_("Path of the picture file for the letters")), lbl_ocr_words(_("Path of the picture file for the words")), b_ocr_letters(Gtk::StockID("gtk-open")), b_ocr_words(Gtk::StockID("gtk-open"))
{
     m_dlg_open_ocr_file=nullptr;
     get_content_area()->set_homogeneous();
     b_OK=add_button(_("OK"), Gtk::RESPONSE_OK);
     b_Cancel=add_button(_("Cancel"), Gtk::RESPONSE_CANCEL);
     hb_ocr_letters.pack_start(e_ocr_letters,Gtk::PACK_SHRINK);
     hb_ocr_letters.pack_start(b_ocr_letters,Gtk::PACK_SHRINK);
     hb_ocr_words.pack_start(e_ocr_words, Gtk::PACK_SHRINK);
     hb_ocr_words.pack_start(b_ocr_words, Gtk::PACK_SHRINK);
     get_content_area()->pack_start(lbl_ocr_letters);
     get_content_area()->pack_start(hb_ocr_letters);
     get_content_area()->pack_start(lbl_ocr_words);
     get_content_area()->pack_start(hb_ocr_words);
     e_ocr_letters.signal_changed().connect(sigc::mem_fun(*this,&Dialog_OCR::on_entries_changed));
     e_ocr_words.signal_changed().connect(sigc::mem_fun(*this,&Dialog_OCR::on_entries_changed));
     b_ocr_letters.signal_clicked().connect(sigc::mem_fun(*this,&Dialog_OCR::on_open_grid_file));
     b_ocr_words.signal_clicked().connect(sigc::mem_fun(*this, &Dialog_OCR::on_open_word_file));
     if (b_OK!=nullptr)
     {
	  b_OK->set_sensitive(false);
     }
     show_all_children();
}

Dialog_OCR::~Dialog_OCR()
{
     delete_dlg_ocr();
}

void Dialog_OCR::on_entries_changed(void)
{
     if ((e_ocr_letters.get_text_length()!=0) && (e_ocr_words.get_text_length()!=0))
     {
	  b_OK->set_sensitive(true);
     }
     else
     {
	  b_OK->set_sensitive(false);
     }
}

void Dialog_OCR::delete_dlg_ocr(void)
{
     if (m_dlg_open_ocr_file!=nullptr)
     {
	  delete m_dlg_open_ocr_file;
	  m_dlg_open_ocr_file=nullptr;
     }
}

int Dialog_OCR::Display_File_Chooser(std::string title)
{
     int result(0);
     if (m_dlg_open_ocr_file==nullptr)
     {
	  m_dlg_open_ocr_file=new Gtk::FileChooserDialog(title,Gtk::FILE_CHOOSER_ACTION_OPEN);
     }
     if (m_dlg_open_ocr_file!=nullptr)
     {
	  m_dlg_open_ocr_file->set_transient_for(*this);
	  m_dlg_open_ocr_file->add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	  m_dlg_open_ocr_file->add_button(Gtk::Stock::OK, Gtk::RESPONSE_OK);
	  auto filter_picture = Gtk::FileFilter::create();
	  filter_picture->set_name(_("pictures files"));
	  filter_picture->add_pattern("*.tiff");
	  filter_picture->add_pattern("*.jpeg");
	  filter_picture->add_pattern("*.png");
	  filter_picture->add_pattern("*.bmp");
	  filter_picture->add_pattern("*.pnm");
	  filter_picture->add_pattern("*.gif");
	  filter_picture->add_pattern("*.webp");
	  filter_picture->add_pattern("*.jp2");
	  m_dlg_open_ocr_file->add_filter(filter_picture);
	  result=m_dlg_open_ocr_file->run();
	  m_dlg_open_ocr_file->hide();
     }
     return result;
}   

void Dialog_OCR::on_open_grid_file(void)
{
     int result=Display_File_Chooser(_("Open a picture for the grid letters"));
     if ((result==Gtk::RESPONSE_OK) && (m_dlg_open_ocr_file->get_filename()!=""))
     {
	  e_ocr_letters.set_text(m_dlg_open_ocr_file->get_filename());
     }
     delete_dlg_ocr();
}

void Dialog_OCR::on_open_word_file(void)
{
     int result=Display_File_Chooser(_("Open a picture for the grid words"));
     if ((result==Gtk::RESPONSE_OK) && (m_dlg_open_ocr_file->get_filename()!=""))
     {
	  e_ocr_words.set_text(m_dlg_open_ocr_file->get_filename());
     }
     delete_dlg_ocr();
}

std::string Dialog_OCR::Get_path_grid_letters()
{
	return e_ocr_letters.get_text();
}

std::string Dialog_OCR::Get_path_words()
{
     return e_ocr_words.get_text();
}
