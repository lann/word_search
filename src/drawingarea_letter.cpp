#include "drawingarea_letter.h"
#include "gdkmm/rgba.h"
#include "cairomm/enums.h"
#include "pangomm/font.h"
#include <iostream>
#include <gdkmm/rgba.h>

DrawingArea_Letter::DrawingArea_Letter()
{
     set_hexpand();
     set_vexpand();
}

DrawingArea_Letter::~DrawingArea_Letter()
{
     
}

bool DrawingArea_Letter::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
     Gtk::Allocation allocation = get_allocation();
     const int width = allocation.get_width();
     const int height = allocation.get_height();
     cr->set_source_rgb(1.0, 1.0, 1.0);
     draw_rectangle(cr, width, height);
     draw_lines(cr, width, height);
     cr->set_source_rgb(0, 0, 0);
     draw_text(cr, width, height);
     return true;
}

void DrawingArea_Letter::draw_rectangle(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height)
{
     cr->rectangle(0, 0, width, height);
     cr->fill();
}

void DrawingArea_Letter::draw_text(const Cairo::RefPtr<Cairo::Context>& cr, int rectangle_width, int rectangle_height)
{
     Pango::FontDescription font;
     font.set_family("Monospace");
     font.set_weight(Pango::WEIGHT_HEAVY);
     font.set_absolute_size(40*Pango::SCALE);
     auto layout = create_pango_layout(m_text);
     layout->get_pixel_size(m_text_width, m_text_height);
     layout->set_font_description(font);
     layout->set_height(rectangle_height);
     layout->set_width(rectangle_width);
     cr->move_to(((double)rectangle_width/2-m_text_width), ((double)rectangle_height/2-m_text_height));
     layout->show_in_cairo_context(cr);
}

void DrawingArea_Letter::draw_lines(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height)
{
     cr->set_line_width((double)width/10);
     cr->set_line_cap(Cairo::LINE_CAP_SQUARE);
     for (size_t i=0; i<m_vec_line.size();i++)
     {
	  Gdk::RGBA color(("#" + m_vec_color[i]));
	  cr->set_source_rgb(color.get_red(), color.get_green(), color.get_blue());
	  switch (m_vec_line.at(i))
	  {
	  case horizontal :
	       cr->move_to(0, (double)(height+m_text_height)/2);
	       cr->line_to(width, (double)(height+m_text_height)/2);
	       break;
	  case vertical:
	       cr->move_to((double)width/2, 0);
	       cr->line_to((double)width/2, height);
	       break;
	  case bottom_left:
	       cr->move_to(0, height);
	       cr->line_to(width, 0);
	       break;
	  case bottom_right:
	       cr->move_to(width,height);
	       cr->line_to(0, 0);
	       break;
	  }
	  cr->stroke();
     }
}

void DrawingArea_Letter::set_text(const Glib::ustring &str)
{
     m_text=str;
}

Glib::ustring DrawingArea_Letter::get_text() const
{
     return m_text;
}

void DrawingArea_Letter::set_color(std::string color)
{
     m_vec_color.push_back(color);
}

void DrawingArea_Letter::set_type_line(Line linetype)
{
     m_vec_line.push_back(linetype);
}
