#ifndef DRAWINGAREA_LETTER_H
#define DRAWINGAREA_LETTER_H
#include "glibmm/ustring.h"
#include <gtkmm/drawingarea.h>
#include <string>
#include <vector>

class DrawingArea_Letter : public Gtk::DrawingArea
{
public:
     DrawingArea_Letter();
     ~DrawingArea_Letter();
     enum Line
     {
	  horizontal,
	  vertical,
	  bottom_left,
	  bottom_right
     };
     void set_text(const Glib::ustring &str);
     Glib::ustring get_text(void) const;
     void set_color(std::string color);
     void set_type_line(Line linetype);
protected:
     bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
private:
     void draw_text(const Cairo::RefPtr<Cairo::Context>& cr, int rectangle_width, int rectangle_height);
     void draw_rectangle(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
     void draw_lines(const Cairo::RefPtr<Cairo::Context>& cr, int width, int height);
     Glib::ustring m_text;
     std::vector<std::string> m_vec_color;
     std::vector<Line> m_vec_line;
     int m_text_width;
     int m_text_height;
};
#endif //DRAWINGAREA_LETTER_H
