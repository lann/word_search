#ifndef WORD_SEARCH_SOLVE_H_INCLUDED
#define WORD_SEARCH_SOLVE_H_INCLUDED
#include "gtkmm/grid.h"
#include <cstddef>
#include <string>
#include <vector>

void set_areadrawing_letter_color_directon_line(int row, int column, int x_direction, int y_direction, size_t length, Gtk::Grid *tab_grid, std::string color);

typedef struct solving
{
     Gtk::Grid *tab_grid, *words_grid;
     size_t row_count, column_count, words_count;
     std::vector<std::string> *vec_sz_color;
} element_solving;

typedef struct tab_explore
{
     bool found, end_grid, end_word;
     char carac;
     int row, column,index_carac;
     int x_direction, y_direction;
} element_explore;

class Word_Search_Solve
{
public:
     Word_Search_Solve();
     Word_Search_Solve(element_solving element);
     ~Word_Search_Solve();
private:
     bool Search_Word(std::string word);
     void Search_Caracter(element_explore &tab_search);
     void Search_First_Caracter(element_explore &tab_search);
     bool Turn_Direction(element_explore &tab_search);
     bool Next_Position_Entry(element_explore &tab_search);
     element_solving m_element;
     tab_explore tab_search;
};
#endif //WORD_SEARCH_SOLVE_H_INCLUDED
