#include "word_search_solve.h"
#include "entry_letter.h"
#include "gtkmm/entry.h"
#include "gtkmm/enums.h"
#include "gtkmm/grid.h"
#include "gtkmm/label.h"
#include "drawingarea_letter.h"
#include <cstddef>
#include <sstream>
#include <string>
#include <gtkmm/messagedialog.h>
#include <libintl.h>
#define _(String) gettext(String)

void set_areadrawing_letter_color_directon_line(int row, int column, int x_direction, int y_direction, size_t length,Gtk::Grid *tab_grid, std::string color)
{
     for (size_t j=0; j<length;j++)
     {
	  Gtk::EventBox *event_tab=(Gtk::EventBox*)tab_grid->get_child_at(column+j*x_direction, row+j*y_direction);;
	  DrawingArea_Letter *label_tab=(DrawingArea_Letter*)get_letter_widget<DrawingArea_Letter>(event_tab);
	  label_tab->set_color(color);
	  if ((x_direction==0) && ((y_direction==-1) || (y_direction==1)) )
	  {
	       label_tab->set_type_line(DrawingArea_Letter::vertical);	    
	  }
	  else if ((y_direction==0) && ((x_direction==1) || (x_direction==-1)))
	  {
	       label_tab->set_type_line(DrawingArea_Letter::horizontal);
	  }
	  else if (((x_direction==1) && (y_direction==-1)) || ((x_direction==-1) && (y_direction==1)))
	  {
	       label_tab->set_type_line(DrawingArea_Letter::bottom_left);
	  }
	  else
	  {
	       label_tab->set_type_line(DrawingArea_Letter::bottom_right);
	  }
     }
}

Word_Search_Solve::Word_Search_Solve()
{

}

Word_Search_Solve::~Word_Search_Solve()
{

}

Word_Search_Solve::Word_Search_Solve(element_solving element) 
{
     bool solving=true;
     m_element=element;
     for (size_t i=0; i<m_element.words_count;i++)
     {
	  Gtk::Label *label_word=(Gtk::Label*) m_element.words_grid->get_child_at(0, i);
	  std::string sz_word=label_word->get_text();
	  if (!(solving=Search_Word(sz_word)))
	  {
	       std::string mes(_("It's impossible to solve the grid. The word "));
	       mes+=(sz_word+(_(" is not present")));
	       Gtk::MessageDialog message(mes,false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
	       message.run();
	  }
	  else
	  {
	       std::string label_markup("<s>");
	       set_areadrawing_letter_color_directon_line(tab_search.row, tab_search.column, tab_search.x_direction, tab_search.y_direction, sz_word.size(), m_element.tab_grid, m_element.vec_sz_color->at(i));
	       label_markup+=(sz_word+"</s>");
	       label_word->set_markup(label_markup);
	  }
     }
}

bool Word_Search_Solve::Search_Word(std::string word)
{
     tab_search.found=false;
     tab_search.end_grid=false;
     tab_search.end_word=false;
     tab_search.row=-1;
     tab_search.column=-1;
     //Loop for the grid
     while ((!tab_search.found) && (!tab_search.end_grid))
     {
	  if (word.size()>0)
	  {
	       //Loop for the word
	       while ((!tab_search.found) && (!(tab_search.end_grid=Next_Position_Entry(tab_search))))
	       {
		    size_t i=1;
		    tab_search.x_direction=0;
		    tab_search.y_direction=-1;
		    tab_search.end_word=false;
		    tab_search.found=false;
		    //Loop for a position
		    while ((!tab_search.end_word) && (!tab_search.found))
		    {
			 tab_search.carac=word.at(0);
			 Search_First_Caracter(tab_search);
			 for (i=1; i< word.size() && !tab_search.end_word && tab_search.found;i++)
			 {
			      tab_search.found=false;
			      tab_search.carac=word.at(i);
			      tab_search.index_carac=i;
			      while ((i==1) && (!tab_search.found) && (!tab_search.end_word))
			      {
				   Search_Caracter(tab_search);
				   if (!tab_search.found)
				   {
					tab_search.end_word=Turn_Direction(tab_search);
				   }
			      }
			      if (i!=1)
			      {
				   Search_Caracter(tab_search);
			      }
			 }
			 if (i==word.size() && (tab_search.found))
			 {
			      tab_search.found=true;
			      tab_search.end_word=false;
			 }
			 else
			 {
			      tab_search.end_word=Turn_Direction(tab_search);
			      tab_search.found=false;
			 }
		    }
		    if (tab_search.end_word)
		    {
			 tab_search.found=false;
		    }
	       }
	  }
	  else
	  {
	       tab_search.end_word=true;
	  }
     }
     return tab_search.found;
}

void Word_Search_Solve::Search_First_Caracter(element_explore &tab_search)
{
     std::string car;
     size_t i, j;
     car=tab_search.carac;
     j=tab_search.column;
     for (i=tab_search.row; i<m_element.row_count && !tab_search.found; i++)
     {
	  for(; j<m_element.column_count && !tab_search.found;j++)
	  {
	       Gtk::EventBox *event_tab=(Gtk::EventBox*)m_element.tab_grid->get_child_at(j,i);
	       DrawingArea_Letter *label_tab=(DrawingArea_Letter*)get_letter_widget<DrawingArea_Letter>(event_tab);
	       if (label_tab->get_text()==car)
	       {
		    tab_search.found=true;
		    tab_search.row=i;
		    tab_search.column=j;
	       }
	  }
	  j=0;
     }
     if ((i==m_element.row_count) && (j==m_element.column_count))
     {
	  tab_search.end_grid=true;
     }
}

void Word_Search_Solve::Search_Caracter(element_explore &tab_search)
{
     size_t row, column;
     row=tab_search.row+tab_search.y_direction*tab_search.index_carac;
     column=tab_search.column+tab_search.x_direction*tab_search.index_carac;
     if ((row<m_element.row_count) && (column<m_element.column_count))
     {
	  Gtk::EventBox *event_tab=(Gtk::EventBox*)m_element.tab_grid->get_child_at(column,row);
	  DrawingArea_Letter *label_tab=(DrawingArea_Letter*)get_letter_widget<DrawingArea_Letter>(event_tab);
	  std::string car;
	  car=tab_search.carac;
	  if (label_tab->get_text()==car)
	  {
	       tab_search.found=true;
	  }
     }
     else
     {
	  tab_search.found=false;
     }
}

// Change the direction of the search (UP, DOWN, LEFT ...)
// Return true if the research sens is out
bool Word_Search_Solve::Turn_Direction(element_explore &tab_search)
{
     bool ret=false;
     if ((tab_search.x_direction==0) && (tab_search.y_direction==-1))
     {
	  tab_search.x_direction=1;
     }
     else if ((tab_search.x_direction==1) && (tab_search.y_direction==-1))
     {
	  tab_search.y_direction=0;
     }
     else if ((tab_search.x_direction==1) && (tab_search.y_direction==0))
     {
	  tab_search.y_direction=1;
     }
     else if ((tab_search.x_direction==1) && (tab_search.y_direction==1))
     {
	  tab_search.x_direction=0;
     }
     else if ((tab_search.x_direction==0) && (tab_search.y_direction==1))
     {
	  tab_search.x_direction=-1;
     }
     else if ((tab_search.x_direction==-1) && (tab_search.y_direction==1))
     {
	  tab_search.y_direction=0;
     }
     else if ((tab_search.x_direction==-1) && (tab_search.y_direction==0))
     {
	  tab_search.y_direction=-1;
     }
     else if ((tab_search.x_direction==-1) && (tab_search.y_direction==-1))
     {
	  ret=true;
     }
     return ret;
}

// Give the next position of the entry tab
// Return true if the position is out of the tab
bool Word_Search_Solve::Next_Position_Entry(element_explore &tab_search)
{
     bool ret=false;
     int row, column;
     if ((tab_search.row==-1) && (tab_search.column==-1))
     {
	  tab_search.row=0;
	  tab_search.column=0;
     }
     else
     {
	  row=tab_search.row;
	  column=tab_search.column+1;
	  if (column>=m_element.column_count)
	  {
	       column=0;
	       row++;
	  }
	  if (row>=m_element.row_count)
	  {
	       ret=true;
	  }
	  else
	  {
	       tab_search.row=row;
	       tab_search.column=column;
	  }
     }

     return ret;
}
