#include "dialog_options.h"
#include "sigc++/functors/mem_fun.h"
#include <libintl.h>
#include <iostream>

#define _(String) gettext(String)

Dialog_Options::Dialog_Options()
{
     Init();
}

Dialog_Options::Dialog_Options(parameters options)
{
     m_options=options;
     e_language.set_text(options.language);
     e_whitelist.set_text(options.whitelist);
     e_blacklist.set_text(options.blacklist);
     chk_ascii.set_active(options.ascii);
     m_btn_dictionary.set_filename(options.dictionary_path);
     Init();
}

Dialog_Options::~Dialog_Options()
{

}

void Dialog_Options::Init(void)
{
     lbl_language.set_text(_("Language"));
     lbl_whitelist.set_text(_("Whitelist of character"));
     lbl_blacklist.set_text(_("Blacklist of character"));
     lbl_path_dictionary.set_text(_("Dictionnary Path"));
     chk_ascii.set_label(_("ASCII character"));
     get_content_area()->set_homogeneous();
     get_content_area()->pack_start(lbl_language);
     get_content_area()->pack_start(e_language);
     get_content_area()->pack_start(lbl_whitelist);
     get_content_area()->pack_start(e_whitelist);
     get_content_area()->pack_start(lbl_blacklist);
     get_content_area()->pack_start(e_blacklist);
     get_content_area()->pack_start(lbl_path_dictionary);
     m_btn_dictionary.set_title(_("Path to the dictionnary file"));
     get_content_area()->pack_start(m_btn_dictionary);
     get_content_area()->pack_start(chk_ascii);
     chk_ascii.signal_toggled().connect(sigc::mem_fun(*this,&Dialog_Options::on_check_button_ascii));
     OK=add_button(_("OK"), Gtk::RESPONSE_OK);
     Cancel=add_button(_("Cancel"), Gtk::RESPONSE_CANCEL);
     OK->signal_clicked().connect(sigc::mem_fun(*this,&Dialog_Options::on_OK));
     show_all_children();
}

void Dialog_Options::on_check_button_ascii(void)
{
     m_options.ascii=!m_options.ascii;
     chk_ascii.set_active(m_options.ascii);
     show_all_children();
}

void Dialog_Options::on_OK(void)
{
     m_options.whitelist=e_whitelist.get_text();
     m_options.blacklist=e_blacklist.get_text();
     m_options.language=e_language.get_text();
     m_options.dictionary_path=m_btn_dictionary.get_filename();
}

parameters Dialog_Options::Get_Options()
{
     return m_options;
}
