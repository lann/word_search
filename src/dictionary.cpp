#include "dictionary.h"
#include "gtkmm/messagedialog.h"
#include <cstddef>
#include <fstream>
#include <ios>
#include <regex>
#include <string>
#include <libintl.h>
#include <vector>

#define _(String) gettext(String)

dictionary::dictionary()
{
     m_valid=false;
}

dictionary::~dictionary()
{
  
}

bool dictionary::is_valid()
{
     return m_valid;
}

void dictionary::test_validity(std::string path)
{
     std::fstream fs;
     fs.open(path,std::ios::in);
     if ((m_valid=fs.good()))
     {
	  m_valid=true;
	  std::string line;
	  dict.clear();
	  while ((std::getline(fs,line)) && (line!=""))
	  {
	       if (line!="")
	       {
		    dict.push_back(line);
	       }
	  }
     }
     else
     {
	  Gtk::MessageDialog message(_("Can't open the dict file"),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE);
     }
     fs.close();
}

size_t dictionary::get_number_words(void)
{
     return dict.size();
}

std::vector<std::string> dictionary::get_words(const std::regex regex)
{
     std::vector<std::string> ret;
     for (size_t i=0; i<dict.size();i++)
     {
	  std::smatch sm;
	  std::regex_match(dict[i],sm,regex);
	  for (size_t j=0;j<sm.size();j++)
	  {
	       ret.push_back(sm[j]);
	  }
     }
     return ret;
}
