#include "entry_letter.h"
#include <cstddef>
#include <gtkmm/cssprovider.h>
#include "gtkmm/eventbox.h"
#include <iostream>
#include <sstream>
#include <string>

Entry_Letter::Entry_Letter()
{
     set_max_length(1);
     set_width_chars(1);
     set_max_width_chars(2);
     set_has_frame();
     add_events(Gdk::ALL_EVENTS_MASK);
}

Entry_Letter::~Entry_Letter()
{
  
}
