#include "dialog_new.h"
#include <libintl.h>
#include <sstream>
#define _(String) gettext (String)

Dialog_New::Dialog_New() : m_lbl_colomn(_("How many columns")), m_lbl_line(_("How many lines")), m_lbl_word(_("How many words")), m_columns_count(0), m_lines_count(0), m_words_count(0)
{
     get_content_area()->set_homogeneous();
     get_content_area()->pack_start(m_lbl_colomn);
     get_content_area()->pack_start(m_e_column);
     get_content_area()->pack_start(m_lbl_line);
     get_content_area()->pack_start(m_e_line);
     get_content_area()->pack_start(m_lbl_word);
     get_content_area()->pack_start(m_e_word);
     bOK=add_button(_("OK"), Gtk::RESPONSE_OK);
     bCancel=add_button(_("Cancel"), Gtk::RESPONSE_CANCEL);
     if (bOK!=nullptr)
     {
	  bOK->set_sensitive(false);
     }
     m_e_column.signal_changed().connect(sigc::mem_fun(*this,&Dialog_New::entry_changed));
     m_e_line.signal_changed().connect(sigc::mem_fun(*this,&Dialog_New::entry_changed));
     m_e_word.signal_changed().connect(sigc::mem_fun(*this,&Dialog_New::entry_changed));
     show_all_children();
}

Dialog_New::~Dialog_New()
{

}
     
unsigned int Dialog_New::Get_Columns_Count()
{
     return m_columns_count;
}

unsigned int Dialog_New::Get_Lines_Count()
{
     return m_lines_count;
}

unsigned int Dialog_New::Get_Words_Count()
{
     return m_words_count;
}

void Dialog_New::entry_changed()
{
     std::istringstream istr_column, istr_line, istr_word;
     istr_column.str(m_e_column.get_text());
     istr_column>>m_columns_count;
     istr_line.str(m_e_line.get_text());
     istr_line>>m_lines_count;
     istr_word.str(m_e_word.get_text());
     istr_word>>m_words_count;
     if ((m_columns_count>0) && (m_lines_count>0) && (m_words_count>0))
     {
	  bOK->set_sensitive();
     }
     else
     {
	  bOK->set_sensitive(false);
     }
}
